// 3. Create a variable getCube and use the exponent operator to compute the cube of a number. (A cube is any number raised to 3)

const getCube = 3 ** 3;
console.log(getCube);

// 4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…

let num = 3;

let message = `The cube ${num} is ${getCube}..`;

console.log(message);

// 5. Create a variable address with a value of an array containing details of an address.
// 6. Destructure the array and print out a message with the full address using Template Literals.

const address = ['258 Washington Ave', 'NW', 'California'];

const [Address1, Address2, Address3] = address;

console.log( `I live at ${Address1} ${Address2}, ${Address3} 90011 `);

// 7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
// 8. Destructure the object and print out a message with the details of the animal using Template Literals.

const animal = {
	animalName: "Lolong",
	animalType: "saltwater crocodile",
	animalweight: 1075,
	animalMeasurement: "20 ft 3 in"
}

console.log(`${animal.animalName} was a ${animal.animalType}. He weighed at ${animal.animalweight} kgs with a measurement of ${animal.animalMeasurement}.`);

// 9. Create an array of numbers.
// 10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
const numbers = [1, 2, 3, 4, 5];
numbers.forEach((numbers) => console.log(`${numbers}`));

// 11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.

const add = (a,b,c,d,e) => a + b + c + d + e;

let total = add(1,2,3,4,5);
console.log(total);

// Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
// 13. Create/instantiate a new object from the class Dog and console log the object.

class Dog{
	constructor (name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}

}
const myDog = new Dog();

myDog.name = "Frankie"
myDog.age = 5;
myDog.breed = "Miniature Dachshund";

console.log(myDog);